<?php wp_footer(); ?>
  <footer class="footer">
    <section class="section-navigation">
        <?php
        $packages = get_field('home_package_packages'); 
        $wp_query = new WP_Query(['post__in' => $packages,'post_type' => 'package', 'orderby' => 'post_date', 'order' => 'ASC']);
        if ($wp_query->have_posts()):
        ?>
        <div class="slides slides--packages" data-options='{"autoplay": true, "infinite": true, "slidesToShow": 8, "responsive": [{"breakpoint": 900, "settings": {"slidesToShow":8}},{"breakpoint": 900, "settings": {"slidesToShow":4}}]}'>
          <?php while ($wp_query->have_posts() ) : $wp_query->the_post(); ?>
            <div class="slides__item">
              <img src="<?php the_post_thumbnail_url(); ?>">
            </div>
          <?php endwhile;?>
        </div>
        <?php endif; wp_reset_query(); ?>
    </section>
    <section class="section-information u-padding-bottom-35 u-padding-top-62">
      <div class="row">
        <div class="logo-ft">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/logo_ft.png""></a>
        </div>
        <p class="info-ft"><span>Headquarters:</span><?php echo get_field('headquarters', 'site_configuration')?></p>
        <p class="info-ft"><span>Branches:</span><?php echo get_field('branches', 'site_configuration')?></p>
        <ul>
          <li>
            <span>Email:</span><?php echo get_field('email', 'site_configuration')?>
          </li>
          <li class="center">
            | 
          </li>
          <li>
            <span>Tel: </span><?php echo get_field('tel', 'site_configuration')?>
          </li>
          <li class="center">
            | 
          </li>
          <li>
            <span>Hotline: </span><?php echo get_field('hotline', 'site_configuration')?>
          </li>
        </ul>
      </div>
    </section>
  </footer>
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/5ccf1de52846b90c57acf817/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
</body>
</html>
