<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
echo get_template_part('includes/top-banner.inc');
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$arrPage = ['dau-tu', 'doanh-nghiep', 'giay-phep', 'linh-vuc-khac', 'nguoi-nuoc-ngoai', 'so-huu-tri-tue'];
			/* Start the Loop */
			while ( have_posts() ) : global $post;
				the_post(); ?>
				<?php $post_name = $post->post_name; 
				if($post_name == 'gioi-thieu' || $post_name == 'about-us'): ?>
					<?php the_content(); ?>
				<?php elseif(in_array($post_name, $arrPage)):?>
					<?php 
						$service = get_term_by('slug', $post_name, 'service');
						$icon = get_field('feature_service', $service->taxonomy.'_'.$service->term_id);
					?>
					<div class="bg-service">
						<div class="row">
							<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['title']; ?>" class="left">
							<div class="info-service left">
								<h1 class="title-service"><?php echo $service->name; ?></h1>
								<p><?php echo $service->description; ?></p>
							</div>
						</div>
					</div>
					<div class="child-service">
						<div class="row">
							<?php
								$paged = get_query_var('paged');
							    $wp_query = new WP_Query(['posts_per_page' => 1, 'post_type' => 'service',
							      'tax_query' => [[
							        'taxonomy' => 'service',
							        'field' => 'slug',
							        'terms' => $service->slug,
							      ]],
							      'paged' => $paged,
							    ]);
							    if ($wp_query->have_posts()):
								    $i = 0;
								    while ($wp_query->have_posts() ) : $wp_query->the_post(); $i++; ?>
										<div class="event-box">
									        <div class="row">
										        <div class="event-box__details">
										            <h5 class="">
										            	<span><?php if($i < 10): echo '0'.$i.'.'; endif; ?></span>
										            	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										            </h5>
										        </div>
									        </div>
									    </div>
									<?php endwhile;?>
							      	<?php 
							          	ddev_pagenavi();
							      	?>
						      	<?php endif;?>
						</div>
					</div>
					<?php break; ?>
				<?php elseif($post_name == 'bieu-mau' || $post_name == 'form'):?>
					<div class="page-form">
						<div class="row">
							<div class="col-1-of-3">
								<h3>Danh Mục Biểu Mẫu</h3>
							</div>
							<div class="col-1-of-3">
								<?php 
									$terms = get_terms( 'form', array(
							            'hide_empty' => false,
							            'order'      => 'ASC',
							            'orderby' => 'term_id',
							          ) );
								?>
								<div class="box-in">
									<select class="select-form">
										<?php foreach ($terms as $key => $item) { ?>
								          	<option value="<?php echo $item->slug; ?>">
								          		<?php echo $item->name; ?>
								          	</option>
								        <?php } ?>
										</select>
								</div>
								
							</div>
							<div class="col-1-of-3">
								<div class="form">
									<input type="text" name="" placeholder="Nhập từ khóa tìm kiếm biểu mẫu" class="input-form-search">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_search_form.jpg">
								</div>
							</div>
						</div>
					</div>
					<div class="page-form-content">
						<div class="row search-result">
							<?php
								$paged = get_query_var('paged');
							    $wp_query = new WP_Query(['posts_per_page' => 9, 'post_type' => 'form',
							      'paged' => $paged,
							    ]);
							    if ($wp_query->have_posts()):
								    $i = 0;
								    global $post;
								    while ($wp_query->have_posts() ) : $wp_query->the_post(); $i++; ?>
										<div class="form-box">
									        <div class="form-box__details">
									            <h5 class="">
									            	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_file.jpg">
									            	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									            	<?php $file = get_field('file_form', $post->ID);
									            		$file = $file['url'];
									            	?>
									            	<a href="<?php echo $file; ?>" class="right" download><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_download.jpg"></a>
									            </h5>
									        </div>
									    </div>
									<?php endwhile;?>
							      	<?php 
							          	ddev_pagenavi();
							      	?>
						      	<?php endif;?>
						</div>
					</div>
					<?php break; ?>
				<?php elseif($post_name == 'tinh-huong-tham-khao' || $post_name == 'form'):?>
					<div class="page-form">
						<div class="row">
							<div class="col-1-of-6">
								<h3>Danh sách tình huống tham khảo</h3>
							</div>
							<div class="col-1-of-6">
								<div class="form">
									<input type="text" name="" placeholder="Nhập từ khóa tìm kiếm" class="input-form-search-event">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_search_form.jpg">
								</div>
							</div>
						</div>
					</div>
					<div class="page-form-content">
						<div class="row search-result">
							<?php
								$paged = get_query_var('paged');
							    $wp_query = new WP_Query(['posts_per_page' => 8, 'post_type' => 'event',
							      'paged' => $paged,
							    ]);
							    if ($wp_query->have_posts()):
								    $i = 0;
								    global $post;
								    while ($wp_query->have_posts() ) : $wp_query->the_post(); $i++; ?>
										<div class="form-box">
									        <div class="form-box__details">
									            <h5 class="">
									            	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/qs.jpg">
									            	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									            	<a href="#" class="right" download><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_de.png"></a>
									            </h5>
									        </div>
									    </div>
									<?php endwhile;?>
							      	<?php 
							          	ddev_pagenavi();
							      	?>
						      	<?php endif;?>
						</div>
					</div>
				<?php break; ?>
				<?php else: ?>
				<div class="row">
					<?php the_content(); ?>
				</div>
				<?php endif; ?>
			<?php
			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
