$(function(){
  /**
   * Navigation Toggle Button
   */
  
  if ($('.navigation__toggle').length) {
    $('.navigation__toggle').on('click', function() {
      let sibling = $(this).siblings('.navigation__list');

      if (sibling.is(':hidden')) {
        sibling.css('display', 'block');
      } else {
        sibling.removeAttr('style');
      }
    });
  }

  /**
   * Slide show
   */
  $('.slides').each(function() {
    let $this = $(this);

    $this.slick($.extend({
      dots          : false,
      autoplay      : true,
      arrows        : false,
      autoplaySpeed : 2000,
      loop          : true
    }, $this.data('options') || {}));
  });

  /**
   * Form Validator
   */
  if ($.validator) {
    $.validator.messages.required = "";
    let form = $('#form');
    let url = form.attr('action');
    form.validate({
      rules: {
        emailaddress: {email: true}
      },

      submitHandler: function(f) {
        let $form = $(f);
        let submitButton = $form.find('button[type="submit"]');
        let tmp = submitButton.text();

        submitButton.text('Sending Form...').prop('disabled', true);

        $.post(url, $form.serialize(), function() {
          form.find('input, textarea').val('');
          submitButton.text(tmp).prop('disabled', false);
          alert('Form Sucessfully Submitted!');
        });

        return false;
      }
    });
  }

  if ($.datepicker) {
    $.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});
    $('.form__datepicker').datepicker();
    $('.form__datepicker--range').each(function() {
      let dateFrom  = $(this).find('.form__datepicker--from');
      let dateTo    = $(this).find('.form__datepicker--to');

      dateFrom.datepicker()
      .on('change', function() {
        let $this = $(this);
        dateTo.datepicker('option', 'minDate', $this.datepicker('getDate'));
        $this.valid();
      })

      dateTo.datepicker()
      .on('change', function() {
        let $this = $(this);
        dateFrom.datepicker('option', 'maxDate', $this.datepicker('getDate'));
        $this.valid();
      });
    });
  }
  $('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');
    
    $({ countNum: $this.text()}).animate({
      countNum: countTo
    },

    {
      duration: 6000,
      easing:'linear',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum);
        //alert('finished');
      }

    });  
  });
  $('.navigation__list .menu-item-has-children').click(function(){
    $(this).children('.sub-menu').toggleClass('toggleSubmenu');
  });
  $(".navigation__toggle").click(function(){
    $(".navigation__list").toggleClass('toggleMobileMenu');
  });
  $(".select-form").click(function(){
    var $this = $(this);
    $.ajax({
      url: '../wp-admin/admin-ajax.php',
      type : "POST",
      data: {
        action: 'search_form',
        form: $this.val(),
        title: $('.input-form-search').val(),
      },
      success: function(response) {
          if(response.success == true){
            $('.search-result').html(response.data);
          }
          else{
            
          }
      },
      error: function() {}
    });
  });
  $(".input-form-search").change(function(){
    var $this = $(this);
    $.ajax({
      url: '../wp-admin/admin-ajax.php',
      type : "POST",
      data: {
        action: 'search_form',
        form: $(".select-form").val(),
        title: $this.val(),
      },
      success: function(response) {
          if(response.success == true){
            $('.search-result').html(response.data);
          }
          else{
            
          }
      },
      error: function() {}
    });
  });

  $(".input-form-search-event").change(function(){
    var $this = $(this);
    $.ajax({
      url: '../wp-admin/admin-ajax.php',
      type : "POST",
      data: {
        action: 'search_event',
        title: $this.val(),
      },
      success: function(response) {
          if(response.success == true){
            $('.search-result').html(response.data);
          }
          else{
            
          }
      },
      error: function() {}
    });
  });
});
