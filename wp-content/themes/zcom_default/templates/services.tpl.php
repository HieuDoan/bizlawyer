<?php /* Template Name: Services */ ?>
<?php get_header(); ?>
  <main>
    <?php echo get_template_part('includes/top-banner.inc'); ?>

    <section class="section-services u-padding-top-100 u-margin-top-25">
      <?php $wp_query = new WP_Query(['posts_per_page' => 4, 'post_type' => 'service']); ?>
      <?php $i = 1; while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
      <div class="services-box u-padding-top-80 <?php echo ($i++ % 2) == 0 ? 'services-box-gray' : 'services-box--right-image'; ?>">
        <div class="row">
          <div class="services-box__image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
          <div class="services-box__details">
            <span class="services-box__sup-header">HAVE PROBLEM WITH</span>
            <h2 class="heading-secondary u-margin-bottom-40 u-margin-top-0"><?php the_title();?></h2>
            <p class="paragraph u-padding-bottom-10"><?php echo get_the_excerpt(); ?></p>
          </div>
        </div>
      </div>
      <?php endwhile;?>

          <div class="u-padding-top-100 u-padding-bottom-100 u-margin-top-45 u-margin-bottom-40"></div>

    </section>

  </main>
<?php get_footer(); ?>
