<?php /* Template Name: Packages */ ?>
<?php get_header(); ?>
<main>
  <?php echo get_template_part('includes/top-banner.inc'); ?>
  <?php foreach (get_terms(['taxonomy' => 'package-type', 'order' => 'DESC']) as $packageType) : 
    ?>
  <section class="section-packages u-padding-top-100 u-padding-bottom-100 <?php if($packageType->slug == 'long-weekend-trips'): echo 'section-packages--gray'; endif;?>">
    <div class="row">
      <div class="blog-title u-padding-bottom-50 u-margin-top-30">
        <?php
        $icon = get_field('package-type-icon', $packageType->taxonomy.'_'.$packageType->term_id);
        if ($icon) : 
        ?>
        <div class="blog-title__img--container">
          <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['title']; ?>" class="section-packages__img"/>
        </div>
        <?php
        endif;
        ?>
        <h1 class="heading-secondary u-text-center u-text-black u-margin-bottom-5 u-margin-top-60"><?php echo $packageType->name; ?></h1>
        <p class="paragraph u-text-center"><?php echo $packageType->description; ?></p>
      </div>
    </div>

    <?php
    $paged = get_query_var('paged');
    $wp_query = new WP_Query(['posts_per_page' => 3, 'post_type' => 'package',
      'tax_query' => [[
        'taxonomy' => 'package-type',
        'field' => 'slug',
        'terms' => $packageType->slug,
      ]],
      'paged' => $paged,
    ]);

    if ($wp_query->have_posts()):
    ?>
    <?php while ($wp_query->have_posts() ) : $wp_query->the_post(); ?>
      <div class="packages-box">
        <div class="row">
          <div class="packages-box__image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
          <div class="packages-box__details">
            <h2 class="heading-secondary u-margin-top-0 u-padding-top-0"><?php echo get_field('price'); ?></h2>
            <h1 class="heading-primary"><?php the_title(); ?></h1>
            <span class="packages-box__sup-header"><?php echo sprintf('%d person %d nights %d star hotel', get_field('person'), get_field('night'), get_field('hotel_star')); ?></span>
            <h2 class="heading-title"><?php echo get_field('tag_line'); ?></h2>

            <p class="paragraph u-text-left"><?php echo get_the_excerpt(); ?></p>
            <div class="packages-box__container--link u-text-left">
              <a href="<?php the_permalink(); ?>" class="packages-box__link">Read More</a>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile;?>
    <div class="row">
      <?php 
        if($packageType->slug == 'long-weekend-trips'):
          ddev_pagenavi();
        endif; 
      ?>
    </div>
    
</div>
    <?php endif;?>
  </section>

  
  <?php endforeach; ?>
  <div class="row">
        <div class="pagination-cover">
        <ul class="pagination-tool">
          <li><a href="#" class="active">1</a></li>
          <li><a href="#">2</a></li>
           <li><a href="#">3</a></li>
        </ul>
        </div>
    </div>

</main>
<?php get_footer(); ?>
