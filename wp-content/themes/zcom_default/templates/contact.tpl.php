<?php /* Template Name: Contact Us */ ?>
<?php get_header(); ?>
<main>
  <?php echo get_template_part('includes/top-banner.inc'); ?>
  <section class="mai-box-contact">
    <div class="row">
        <div class="col-w-6">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_mail.png" class="left">
          <div class="left">
            <h1>đăng ký nhận bản tin pháp luật</h1>
            <p>
              Nhận thông tin mới nhất được gửi vào hộp thư của bạn
            </p>
          </div>
        </div>
        <div class="col-w-6">
            <form id="form" class="form form--contact-headding" action="<?php echo get_template_directory_uri(); ?>/send-mail.php">
                <input type="text" class="emailaddress" name="emailaddress" placeholder="Nhập email của bạn" required />
                <button type="submit" class=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/submit-heading.png"></button>
                <input type="hidden" name="form-type" value="contact-form-headding" />
            </form>
        </div>
    </div>
  </section>
  <section class="section-contact--box">
    <div class="row margin-top-55 u-padding-top-70 u-margin-bottom-100 u-padding-bottom-100">
      <div class="col-1-of-2 u-margin-right-0 block-left-contact">
        <h4>Thông tin liên hệ</h4>
        <p><?php echo get_field('info_contact'); ?></p>
        <p class="tel">
          <?php echo get_field('tel_contact'); ?>
        </p>
        <p class="email">
          <?php echo get_field('email_contact'); ?>
        </p>
        <ul class="list_social">
          <li>
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_in.jpg"></a>
          </li>
          <li>
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_tw.jpg"></a>
          </li>
          <li>
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_fb.jpg"></a>
          </li>
          <li>
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_youtube.png"></a>
          </li>
        </ul>
      </div>
      <div class="col-1-of-2 block-right-contact">
        <div class="quotation-form--contact">
          <form id="form" class="form form--contact" action="<?php echo get_template_directory_uri(); ?>/send-mail.php">
            <h1 class="form__title">Đăng ký tư vấn</h1>

            <div class="form__field">
              <div class="row">
                  <input type="text" name="emailaddress" placeholder="Nhập email hoặc số điện thoại" required />
              </div>
            </div>
            <div class="form__field">
              <div class="row">
                  <textarea type="text" rows="6" name="message" placeholder="Nhập nội dung bạn cần chúng tôi tư vấn" required></textarea>
              </div>
            </div>

            <div class="form__field">
              <div class="row">
                <button type="submit" class="button button--primary u-right"></button>
                <input type="hidden" name="form-type" value="contact-form" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</main>
<?php get_footer(); ?>
