<?php
require_once '../../../wp-load.php';

$to = get_field('email', 'site_configuration');
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($to)) {
  $headers  = array(
    'Content-Type: text/html; charset=UTF-8',
    'From: '.get_bloginfo('name').' <'.$to.'>'
  );

  switch (filter_input(INPUT_POST, 'form-type')) {

    case 'contact-form-headding' :
      $subject  = 'Đăng ký nhận bản tin pháp luật';
      $message  = '

      <table border="0">
        <tr>
          <td><strong>Email Address</strong></td>
          <td>: '.filter_input(INPUT_POST, 'emailaddress').'</td>
        </tr>
      </table>

      ';
    break;

    case 'contact-form' :
      $subject = 'Contact Us';
      $message  = '

      <table border="0">
        <tr>
          <td><strong>Email Address</strong></td>
          <td>: '.filter_input(INPUT_POST, 'emailaddress').'</td>
        </tr>
        <tr>
          <td><strong>Message</strong></td>
          <td>: '.filter_input(INPUT_POST, 'message').'</td>
        </tr>
      </table>

      ';
    break;
  }

}

echo json_encode(array('send' => wp_mail($to, $subject, $message, $headers)));
