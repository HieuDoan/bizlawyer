<?php $slug = basename(get_permalink()); ?>
<section class="top-banner <?php echo "top-banner-".$slug; ?>" style="background-image: url(<?php echo get_field('tp_background_image'); ?>);">
  <div class="row">
    <h1 class="heading-primary u-text-white"><?php echo get_field('tp_title'); ?></h1>
    <p class="paragraph u-text-white"><?php echo get_field('tp_description'); ?></p>
	<?php if(is_front_page()): ?>
	    <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="form-search-banner">
	      <input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'Tìm kiếm thông tin, thủ tục, giấy phép bạn đang quan tâm...', 'twentyeleven' ); ?>" />
	      <input type="submit" class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( '', 'twentyeleven' ); ?>" />
    	</form>
	<?php endif; ?>
  </div>
</section>