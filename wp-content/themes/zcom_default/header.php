<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

  <header class="header">
    <div class="top-header">
      <div class="left">
          <img src="<?php if(get_locale() == 'vi'): echo 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFsSURBVHjaYvzPgAD/UNlYEUAAmuTYAAAQhAEYqF/zFbe50RZ1cMmS9TLi0pJLRjZohAMTGFUN9HdnHgEE1sDw//+Tp0ClINW/f4NI9d////3+f+b3/1+////+9f/XL6A4o6ws0AaAAGIBm/0fRTVQ2v3Pf97f/4/9Aqv+DdHA8Ps3UANAALEAMSNQNdDGP3+ALvnf8vv/t9//9X/////7f+uv/4K//iciNABNBwggsJP+/IW4kuH3n//1v/8v+wVSDURmv/57//7/CeokoKFA0wECiAnkpL9/wH4CO+DNr/+VQA1A9PN/w6//j36CVIMRxEkAAQR20m+QpSBXgU0CuSTj9/93v/8v//V/xW+48UBD/zAwAAQQSAMzOMiABoBUswCd8ev/M7A669//OX7///Lr/x+gBlCoAJ0DEEAgDUy//zBISoKNAfoepJNRFmQkyJecfxj4/kDCEIiAigECiPErakTiiWMIAAgwAB4ZUlqMMhQQAAAAAElFTkSuQmCC'; else: echo 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAflJREFUeNpinDRzn5qN3uFDt16+YWBg+Pv339+KGN0rbVP+//2rW5tf0Hfy/2+mr99+yKpyOl3Ydt8njEWIn8f9zj639NC7j78eP//8739GVUUhNUNuhl8//ysKeZrJ/v7z10Zb2PTQTIY1XZO2Xmfad+f7XgkXxuUrVB6cjPVXef78JyMjA8PFuwyX7gAZj97+T2e9o3d4BWNp84K1NzubTjAB3fH0+fv6N3qP/ir9bW6ozNQCijB8/8zw/TuQ7r4/ndvN5mZgkpPXiis3Pv34+ZPh5t23//79Rwehof/9/NDEgMrOXHvJcrllgpoRN8PFOwy/fzP8+gUlgZI/f/5xcPj/69e/37//AUX+/mXRkN555gsOG2xt/5hZQMwF4r9///75++f3nz8nr75gSms82jfvQnT6zqvXPjC8e/srJQHo9P9fvwNtAHmG4f8zZ6dDc3bIyM2LTNlsbtfM9OPHH3FhtqUz3eXX9H+cOy9ZMB2o6t/Pn0DHMPz/b+2wXGTvPlPGFxdcD+mZyjP8+8MUE6sa7a/xo6Pykn1s4zdzIZ6///8zMGpKM2pKAB0jqy4UE7/msKat6Jw5mafrsxNtWZ6/fjvNLW29qv25pQd///n+5+/fxDDVbcc//P/zx/36m5Ub9zL8+7t66yEROcHK7q5bldMBAgwADcRBCuVLfoEAAAAASUVORK5CYII='; endif;?>"><?php pll_the_languages(array('dropdown'=>1)); ?>
          <div class="right">
            <ul>
              <li>
                <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/i_in.png"></a>
              </li>
              <li>
                <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/i_tw.png"></a>
              </li>
              <li>
                <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/i_fb.png"></a>
              </li>
              <li>
                <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/i_yt.png"></a>
              </li>
            </ul>
          </div>
      </div>
      <div class="right">
        <a href="tel: 0868881900"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/phone.png">+(84) 86 888 1900</a>
      </div>
    </div>
    <div class="bottom-header">
      <div class="left">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/header/logo.png" alt="Template 3" class="header__logo img-responsive">
        </a>
      </div>
      <nav class="navigation navigation--header right">
        <div class="navigation__toggle">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <?php 
          if ( has_nav_menu( 'header-menu' ) ) :
            wp_nav_menu(
              array(
                'theme_location' => 'header-menu',
                'menu_class' => 'navigation__list',
              )
            );
          endif;
        ?>
        <!-- <ul class="navigation__list">
          <?php 
          if(get_menu_list('header-menu')): foreach (get_menu_list('header-menu') as $header_menu) : ?>
          <li class="navigation__item <?php if ((is_home() && $header_menu->post_name === 'home') || is_page($header_menu->object_id)) : ?>active<?php endif; ?>"><a href="<?php echo $header_menu->url; ?>"><?php echo $header_menu->title; ?></a></li>
          <?php endforeach; endif;?>
        </ul> -->
        <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="right">
          <input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( '', 'twentyeleven' ); ?>" />
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-s1.png">
        </form>
      </nav>
    </div>
  </header>
