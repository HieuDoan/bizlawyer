<?php get_header(); ?>
<main>
  <?php echo get_template_part('includes/top-banner.inc'); ?>
  <section class="section-service">
    <h1>Lĩnh vực dịch vụ</h1>
    <div class="row">
      <div class="row-index">
        <?php
          $terms = get_terms( 'service', array(
            'hide_empty' => false,
            'order'      => 'ASC',
            'orderby' => 'term_id',
          ) );
        ?>
        <?php foreach ($terms as $key => $item) { ?>
          <div class="col-1-of-3">
            <div class="service-card">
              <div class="service-card__image">
                <?php $src = get_field('feature_service', $item->taxonomy.'_'.$item->term_id); ?>
                <img src="<?php echo $src['url']; ?>">
                <a href="<?php the_permalink(); ?>"></a>
              </div>
              <div class="service-card__title"><a href="/<?php echo $item->slug; ?>" class=""><?php echo $item->name; ?></a></div>
              <div class="service-card__details">
                <div class="service-card__description">
                  <?php echo $item->description; ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>
    </div>

  </section>

  <section class="section-about-us u-padding-bottom-20">
    <div class="row">
      <div class="services-box">
        <div class="services-box__details">
          <span class="services-box__sup-header">Các bước</span>
          <h2 class="services-box__title"><?php echo get_field('home_about_title'); ?></h2>
          <ul class="services-box__list-service">
            <li>
              <div class="list-service__title"><span class="number">1</span>Tiếp nhận yêu cầu <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_down.png"></div>
              <div class="description"><?php echo get_field('home_about_description_1'); ?></div>
            </li>
            <li>
              <div class="list-service__title"><span class="number">2</span>Tư vấn tổng quan và báo giá dịch vụ <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_up.png"></div>
              <div class="description showContent"><?php echo get_field('home_about_description_2'); ?></div>
            </li>
            <li>
              <div class="list-service__title"><span class="number">3</span>Ký kết hợp đồng <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_down.png"></div>
              <div class="description"><?php echo get_field('home_about_description_3'); ?></div>
            </li>
            <li>
              <div class="list-service__title"><span class="number">4</span>Tiếp nhận & xử lý hồ sơ <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_down.png"></div>
              <div class="description"><?php echo get_field('home_about_description_4'); ?></div>
            </li>
            <li>
              <div class="list-service__title"><span class="number">5</span>Trả kết quả <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_down.png"></div>
              <div class="description"><?php echo get_field('home_about_description_5'); ?></div>
            </li>
            <li>
              <div class="list-service__title"><span class="number">6</span>Hỗ trợ pháp lý <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_down.png"></div>
              <div class="description"><?php echo get_field('home_about_description_6'); ?></div>
            </li>
          </ul>
        </div>
        <div class="services-box__image--container" style="background-image: url(<?php echo get_field('home_about_image')['url']; ?>);"></div>
      </div>
    </div>
  </section>
  <section class="statistical">
    <div class="row">
      <ul class="statistical__list">
        <li>
          <img src="<?php echo get_field('image_statistical_1')['url']; ?>">
          <h3 class="counter" data-count="<?php echo get_field('number_statistical_1'); ?>">0</h3>
          <h4>
            <?php echo get_field('title_statistical1'); ?>
          </h4>
        </li>
        <li>
          <img src="<?php echo get_field('image_statistical_2')['url']; ?>">
          <h3 class="counter" data-count="<?php echo get_field('number_statistical_2'); ?>">0</h3>
          <h4>
            <?php echo get_field('title_statistical_2'); ?>
          </h4>
        </li>
        <li>
          <img src="<?php echo get_field('image_statistical_3')['url']; ?>">
          <h3><span class="counter" data-count="<?php echo get_field('number_statistical_3'); ?>">0</span>+</h3>
          <h4>
            <?php echo get_field('title_statistical_3'); ?>
          </h4>
        </li>
        <li>
          <img src="<?php echo get_field('image_statistical_4')['url']; ?>">
          <h3><span class="counter" data-count="<?php echo get_field('number_statistical_4'); ?>">0</span>+</h3>
          <h4>
            <?php echo get_field('title_statistical_4'); ?>
          </h4>
        </li>
      </ul>
    </div>
  </section>
  <section class="event">
    <div class="row">
        <div class="event-box">
            <span class="event-box__sup-header">Các tình huống</span>
            <h2 class="event-box__title">Tham khảo</h2>
            <div class="wrapper">
              <div class="slick-event">
                <?php 
                  $wp_query = new WP_Query(['post_type' => 'event']);
                  if ($wp_query->have_posts()): global $post;
                    while ($wp_query->have_posts() ) : $wp_query->the_post();
                    ?>
                      <div class="slick-event__main">
                        <a href="<?php the_permalink(); ?>">
                          <img src="<?php the_post_thumbnail_url(); ?>">
                        </a>
                        <p class="slick-event__main__the_date"><?php echo get_the_date( 'd/m/Y', $post->ID ); ; ?></p>
                        <p class="slick-event__main__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                        <p class="slick-event__main__expert">
                          <?php echo get_the_excerpt(); ?>
                        </p>
                        <p class="slick-event__main__read_more"><a href="<?php the_permalink(); ?>">ĐỌC THÊM</a></p>
                      </div>
                    <?php endwhile;?>
                <?php endif ?>
              </div>
            </div>
            <p class="text-center text-more"><a href="">Tham khảo các tình huống khác</a></p>
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.slick-event').slick({
                    slidesToShow: '<?php if(wp_is_mobile()): echo "1"; else: echo '4'; endif; ?>',
                    slidesToScroll: 1,
                    autoplay: false,
                    nextArrow: '<img class="next" src="<?php echo get_template_directory_uri(); ?>/assets/images/common/next.png; ?>" >',
                    prevArrow: '<img class="prev" src="<?php echo get_template_directory_uri(); ?>/assets/images/common/prev.png; ?>" ><img class="center" src="<?php echo get_template_directory_uri(); ?>/assets/images/common/xie.png; ?>" >',
                  });
                });
              </script>
        </div>
    </div>
  </section>
</main>
<?php get_footer(); ?>
<script type="text/javascript">
  $(function(){
    $(".services-box__list-service li").click(function(){
      var current = $(this);
      if(!current.children('div.description').hasClass('showContent')){
        current.children('div.description').addClass('showContent');
        current.children('.list-service__title').children('img').attr('src', '<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_up.png');
      }else{
        current.children('div.description').removeClass('showContent');
        current.children('.list-service__title').children('img').attr('src', '<?php echo get_template_directory_uri(); ?>/assets/images/common/icon_down.png');
      }
    });
  });
</script>
