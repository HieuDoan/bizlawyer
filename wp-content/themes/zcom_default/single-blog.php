<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<main>
  <section class="section-content">
    <div class="row">
      <h1 class="heading-primary u-padding-bottom-10"><?php  the_title();?></h1>
      <?php echo the_content(); ?>
    </div>
  </section>
</main>
<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
