<?php
// Include Global functions
require_once dirname(__FILE__).'/../../common/functions.php';

if (function_exists('acf_add_options_page')) {
	acf_add_options_page([
    'page_title'  => 'Site Configuration',
    'position'    => 2,
    'post_id'     => 'site_configuration'
  ]);
}

// Package Menu
add_custom_post('package', 'Connect', ['menu_icon' => 'dashicons-admin-post', 'public' => true]);

// Services Menu
add_custom_post('service', 'Dịch vụ', ['menu_icon' => 'dashicons-admin-post']);
add_custom_post('form', 'Biểu mẫu', ['menu_icon' => 'dashicons-admin-post']);

add_custom_post('event', 'Tình huống', ['menu_icon' => 'dashicons-admin-post']);

$labels = array(
    'name'                       => _x( 'Danh mục', 'taxonomy general name', 'textdomain' ),
    'singular_name'              => _x( 'Danh mục', 'taxonomy singular name', 'textdomain' ),
    'search_items'               => __( 'Search Danh mục', 'textdomain' ),
    'popular_items'              => __( 'Popular Danh mục', 'textdomain' ),
    'all_items'                  => __( 'All Danh mục', 'textdomain' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Danh mục', 'textdomain' ),
    'update_item'                => __( 'Update Danh mục', 'textdomain' ),
    'add_new_item'               => __( 'Add New Danh mục', 'textdomain' ),
    'new_item_name'              => __( 'New Danh mục Name', 'textdomain' ),
    'separate_items_with_commas' => __( 'Separate Danh mục with commas', 'textdomain' ),
    'add_or_remove_items'        => __( 'Add or remove Danh mục', 'textdomain' ),
    'choose_from_most_used'      => __( 'Choose from the most used Danh mục', 'textdomain' ),
    'not_found'                  => __( 'No Danh mục found.', 'textdomain' ),
    'menu_name'                  => __( 'Danh mục', 'textdomain' )
);

$args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'service' ),
    );

register_taxonomy( 'service', 'service', $args );


$labelForm = array(
    'name'                       => _x( 'Danh mục', 'taxonomy general name', 'textdomain' ),
    'singular_name'              => _x( 'Danh mục', 'taxonomy singular name', 'textdomain' ),
    'search_items'               => __( 'Search Danh mục', 'textdomain' ),
    'popular_items'              => __( 'Popular Danh mục', 'textdomain' ),
    'all_items'                  => __( 'All Danh mục', 'textdomain' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Danh mục', 'textdomain' ),
    'update_item'                => __( 'Update Danh mục', 'textdomain' ),
    'add_new_item'               => __( 'Add New Danh mục', 'textdomain' ),
    'new_item_name'              => __( 'New Danh mục Name', 'textdomain' ),
    'separate_items_with_commas' => __( 'Separate Danh mục with commas', 'textdomain' ),
    'add_or_remove_items'        => __( 'Add or remove Danh mục', 'textdomain' ),
    'choose_from_most_used'      => __( 'Choose from the most used Danh mục', 'textdomain' ),
    'not_found'                  => __( 'No Danh mục found.', 'textdomain' ),
    'menu_name'                  => __( 'Danh mục', 'textdomain' )
);

$argForm = array(
    'hierarchical'          => true,
    'labels'                => $labelForm,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'service' ),
    );

register_taxonomy( 'form', 'form', $argForm );
// Blog Menu
//add_custom_post('blog', 'Blog', ['menu_icon' => 'dashicons-id-alt', 'public' => true]);

add_action('after_setup_theme', '_template_after_setup_theme'); function _template_after_setup_theme() {
  add_action('wp_enqueue_scripts', '_template_wp_enqueue_scripts'); function _template_wp_enqueue_scripts() {
    wp_enqueue_script('jquery-js', get_node_modules_file_url().'/jquery/dist/jquery.min.js');

    // Slick Carousel
    wp_enqueue_style('slick-carousel-css', get_node_modules_file_url().'/slick-carousel/slick/slick.css');
    wp_enqueue_script('slick-carousel-js', get_node_modules_file_url().'/slick-carousel/slick/slick.min.js');

    // Font Awesome
    wp_enqueue_style('font-awesome-css', get_node_modules_file_url().'/font-awesome/css/font-awesome.min.css');

    // Main Javascript
    wp_enqueue_script('main-js', get_template_directory_uri().'/assets/js/main.js');

    // Magnific Popup
    wp_enqueue_style('magnific-popup-css', get_node_modules_file_url().'/magnific-popup/dist/magnific-popup.css');
    wp_enqueue_script('magnific-popup-js', get_node_modules_file_url().'/magnific-popup/dist/jquery.magnific-popup.min.js');

    // Jquery Validation
    wp_enqueue_script('jquery-validation-js', get_node_modules_file_url().'/jquery-validation/dist/jquery.validate.min.js');

    // Jquery UI
    wp_enqueue_style('jquery-ui-css', get_template_directory_uri().'/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css');
    wp_enqueue_style('jquery-ui-theme-css', get_template_directory_uri().'/assets/plugins/jquery-ui-1.12.1/jquery-ui.theme.min.css');
    wp_enqueue_script('jquery-ui-js', get_template_directory_uri().'/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js');
  }
}

require_once ( get_template_directory() . '/functions/pagenavi.php');

function ddev_pagenavi( $query = false, $num = false ){
    echo '<div class="ddev-pagenavi">'; 
        ddev_get_pagenavi( $query, $num );
    echo '</div>';
}

add_filter( 'pll_get_post_types', 'add_cpt_to_pll', 10, 2 );
 
function add_cpt_to_pll( $post_types, $is_settings ) {
    if ( $is_settings ) {
        
        unset( $post_types['service'] );
    } else {
        
        $post_types['service'] = 'service';
    }
    return $post_types;
}

function add_classes_on_li($classes, $item, $args) {
  $classes[] = 'navigation__item';
  return $classes;
}
add_filter('nav_menu_css_class','add_classes_on_li',1,3);

add_action( 'wp_ajax_search_form', 'search_form_control' );
add_action( 'wp_ajax_nopriv_search_form', 'search_form_control' );
function search_form_control() {
    $term = $_POST['form'];
    $title = $_POST['title'];
    $wp_query = new WP_Query(
        ['posts_per_page' => 9, 
        'post_type' => 'form',
        'tax_query' => [[
            'taxonomy' => 'form',
            'field' => 'slug',
            'terms' => $term,
        ]],
        's' => $title,
    ]);
    $data = '';
    if ($wp_query->have_posts()):
        $i = 0;
        global $post;
        while ($wp_query->have_posts() ) : $wp_query->the_post(); $i++; 
            $file = get_field('file', $post->ID);
            $file = $file['url'];
            $link = get_permalink($post->ID);
            $data .= '<div class="form-box">';                
            $data .= '<div class="form-box__details">';
            $data .= '<h5 class="">';
            $data .= '<img src="'.get_template_directory_uri().'/assets/images/common/icon_file.jpg">';
            $data .= '<a href="'.$link.'">'.$post->post_title.'</a>';
            $data .= '<a href="'.$file.'" class="right" download><img src="'.get_template_directory_uri().'/assets/images/common/icon_download.jpg"></a>';
            $data .= '</h5>';
            $data .= '</div>'; 
            $data .= '</div>'; ?>
        <?php endwhile;
    endif;
    wp_send_json_success($data);
    die();
}

add_action( 'wp_ajax_search_event', 'search_event_control' );
add_action( 'wp_ajax_nopriv_search_event', 'search_event_control' );
function search_event_control() {
    $title = $_POST['title'];
    $wp_query = new WP_Query(
        ['posts_per_page' => 9, 
        'post_type' => 'event',
        's' => $title,
    ]);
    $data = '';
    if ($wp_query->have_posts()):
        $i = 0;
        global $post;
        while ($wp_query->have_posts() ) : $wp_query->the_post(); $i++; 
            $link = get_permalink($post->ID);
            $data .= '<div class="form-box">';                
            $data .= '<div class="form-box__details">';
            $data .= '<h5 class="">';
            $data .= '<img src="'.get_template_directory_uri().'/assets/images/common/qs.jpg">';
            $data .= '<a href="'.$link.'">'.$post->post_title.'</a>';
            $data .= '<a href="#" class="right" download><img src="'.get_template_directory_uri().'/assets/images/common/icon_de.png"></a>';
            $data .= '</h5>';
            $data .= '</div>'; 
            $data .= '</div>'; ?>
        <?php endwhile;
    endif;
    wp_send_json_success($data);
    die();
}
?>